<?php
/**
 * Copyright (C) 2013 peredur.net
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'includes/reset.inc.php';

sec_session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Password Reset</title>
        <link rel="stylesheet" href="styles/main.css" />
		<script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
    </head>
    <body>
        <?php if (login_check($mysqli) == true) : ?>
        <p>Welcome <?php echo htmlentities($_SESSION['username']); ?>!</p>
            <p>
                This is your personal password management page.
            </p>
            <p>Return to <a href="login_here.php">login page</a></p>
	    <a href="includes/logout.php">Logout</a>
	    



	    <form method="post" name="reset_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
		<input type="text" name="username" id="username" value="<?php echo htmlentities($_SESSION['username']); ?>"  readonly>
            New Password: <input type="password" name="password" id="password"/><br>
            Confirm password: <input type="password" name="confirmpwd" id="confirmpwd" /><br>
            <input type="button" value="Change" onclick="return resetformhash(this.form,
                                   this.form.username,
                                   this.form.password,
                                   this.form.confirmpwd);" /> 
        </form>







        <?php else : header( "Location: 403.php" ); ?>
            <p>
                <span class="error">You are not authorized to access this page.</span> Please <a href="login_here.php">login</a>.
            </p>
        <?php endif; ?>





    </body>
</html>
