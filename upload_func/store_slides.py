import sys
import MySQLdb
import os
import subprocess
import re

import rake
import operator

stops = set(['all', 'just', 'being', 'over', 'both', 'through', 'yourselves', 'its', 'before', 'herself', 'had', 'should', 'to', 'only', 'under', 'ours', 'has', 'do', 'them', 'his', 'very', 'they', 'not', 'during', 'now', 'him', 'nor', 'did', 'this', 'she', 'each', 'further', 'where', 'few', 'because', 'doing', 'some', 'are', 'our', 'ourselves', 'out', 'what', 'for', 'while', 'does', 'above', 'between', 't', 'be', 'we', 'who', 'were', 'here', 'hers', 'by', 'on', 'about', 'of', 'against', 's', 'or', 'own', 'into', 'yourself', 'down', 'your', 'from', 'her', 'their', 'there', 'been', 'whom', 'too', 'themselves', 'was', 'until', 'more', 'himself', 'that', 'but', 'don', 'with', 'than', 'those', 'he', 'me', 'myself', 'these', 'up', 'will', 'below', 'can', 'theirs', 'my', 'and', 'then', 'is', 'am', 'it', 'an', 'as', 'itself', 'at', 'have', 'in', 'any', 'if', 'again', 'no', 'when', 'same', 'how', 'other', 'which', 'you', 'after', 'most', 'such', 'why', 'a', 'off', 'i', 'yours', 'so', 'the', 'having', 'once'])

optimize = True

def parsexml(filepath):
	print "came here"
	txtDict = {}

	#stops = set(stopwords.words('english'))	
	size=0

	try:
		with open(filepath , 'r') as myfile:
			print "errored"
			for line in myfile:
				matchobject = re.match(r'<text.*size="([0-9.]*)">([A-Za-z0-9 \s]*)</text>' , line , re.M|re.I)
				if matchobject:
					size = int(float(matchobject.group(1)))
					text = str(matchobject.group(2))				
					if size in txtDict:
						txtDict[size] += text.lower()
					else:
					 	txtDict[size] = text.lower()	

				if line == "<text> </text>\n" or line == "<text>\n":
					txtDict[size]+=' '

	except:
		print "Unable to open file " + filepath

	return txtDict

def get_Heading(Mydict):
	heading = ""
	if len(Mydict.keys()) >= 2:
		max_key = max(Mydict.keys(), key=int)
		heading = Mydict[max_key].split()
	else:
		if (len(Mydict[Mydict.keys()[0]]) < 5):
			heading =  Mydict[Mydict.keys()[0]].split()
		else:
			heading =  []

	heading_new = []
	global stops

	for item in list(set(heading)):
		if item not in stops:
			heading_new.append(item)

	return heading_new

def get_meta_information(Mydict):
	max_key = max(Mydict.keys(), key=int)
	min_key = min(Mydict.keys(), key=int)

	meta = ""
	for item in Mydict:
		if item <= min_key*0.7  + max_key*0.3 and not (item == max_key):
			meta += Mydict[item]

	meta =  list(set(meta.split()))
	meta_new = []

	global stops

	for item in meta:
		if item not in stops:
			meta_new.append(item)

	return meta_new

'''
def get_text_body(Mydict):
	body = ""
	body_data = ""

	for key in Mydict:
		body += Mydict[key] + ". "
		body_data += Mydict[key] + " "

	#body is a string
	if optimize:
		rake_object = rake.Rake("SmartStoplist.txt",4,5,1)

		rake_output = rake_object.run(body)

		if(rake_output[0][1] == rake_output[-1][1]):
			body = body_data
		else:
			body = ""
			for item in rake_output:
				if (item[1] > rake_output[-1][1]):
					body += item[0] + " "
	else:
		body = body_data

	body = body.split()

	body =  list(set(body) - set(get_Heading(Mydict)) - set(get_meta_information(Mydict)))

	return body 
'''	


is_pdf = True


def sha1OfFile(filepath):
    try:
        import hashlib
        sha = hashlib.sha1()
        with open(filepath, 'rb') as f:
            while True:
                block = f.read(1024) # Magic number: one-megabyte blocks.
                if not block: break
                sha.update(block)
            return sha.hexdigest()
    except:
        return str(0)

filepath=str(sys.argv[1])
print "filepath"+str(sys.argv[1])

db = MySQLdb.connect(host="localhost",user="root",passwd="root", db="secure_login")

extension = filepath.split('.')[1]
file_core=filepath.split('.')[0]
print "extension"+extension
if not(extension == "pdf"):
    is_pdf = False
    # convert to pdf
    os.system("unoconv -f pdf "+"../"+filepath)
    print "converted to pdf"

# sha1_file_pdf=sha1OfFile("../"+file_core+".pdf")
# print "Pdf file "+filecd _core+".pdf"
sha1_file = sha1OfFile("../"+filepath)


cur = db.cursor()

query = "SELECT * from doc_names WHERE doc_sha1 like \""+ sha1_file+"\""
print query
cur.execute(query)

if cur.rowcount == 0:
    print "New doc"
    os.system("cp "+ "../" + file_core+".pdf "+ "../upload/" + sha1_file+".pdf")   # creating sha1.pdf
 
    query="pdftk ../upload/"+sha1_file+".pdf dump_data | grep NumberOfPages"
    NumOfPages = subprocess.check_output(query ,shell=True)
    NumOfPages=NumOfPages.split(':')[1]
    NumOfPages=int(NumOfPages)
 

    query = "INSERT INTO doc_names VALUES (\""+filepath+"\",\""+sha1_file+"\","+str(NumOfPages)+")"
    print query
    cur.execute(query)
    db.commit()

# burst the pdf file obtained in a folder sha
    # create a folder where the burst output will be stored
    directory="../upload/Folder_"+sha1_file+"/"
    try:
        os.stat(directory)

    except:
        os.mkdir(directory)       

    statement="pdftk ../upload/"+sha1_file+".pdf burst output "+directory+sha1_file+"_%000d.pdf"
    print "PDFBURST : "+statement
    os.system(statement)
    print "PDF created---------------------------"
    # original file ../upload/sha1_file.pdf has individual files as pdf in ../upload/Folder_sha1_file_%000d.pdf

    # create doc vs slide table, 1st column would be doc_sha, second column would be file sha _ access the file using ../upload/data/filesha.pdf

    # doc = sha1_file, 2nd colum = create sha of the file , 3 column would be the slide number

    for i in xrange(1,NumOfPages+1):
        doc_sha = sha1_file
        ref_slide_i = directory+sha1_file+"_"+str(i)+".pdf"
        slide_sha = sha1OfFile(ref_slide_i)
        query = "INSERT INTO doc_vs_slide VALUES (\""+ filepath +"\",\"" + slide_sha + "\"," + str(i)+")"            # doc_sha, slide_sha, slide_no stored
        print query
        cur.execute(query)
        db.commit()
        # move the created slide to data folder, also rename the slide as sha.pdf
        data_path = "../upload/data/"
        try:
            os.stat(data_path)
        except:
            os.mkdir(data_path)
        # data folder will store all the individual files, for later access of individual pdf, it will be through this folder
        statement = "mv "+ref_slide_i+" "+data_path+slide_sha+".pdf" # file will be stored into data folder, if already there will be overwritten
        

        os.system(statement)
        print "CP command performed, do Move command later on, need to delete pdf in directory+sha1_fileNO.pdf"
        # all files to be accessed later will be done through the upload/data/ folder "filename= sha1_slide.pdf 
        # do here update features corresponding to this slide in the feature table to be made 
        # slide_where_feature_extraction_to_be_done = data_path+slide_sha+".pdf" 
        # 


        ##EXTRACTING INFORMATION

        curr_slide_path = data_path + slide_sha + ".pdf"

        #Convert into xml
        statement = "pdf2txt -o "  + data_path +  "out.xml -t xml " +  curr_slide_path

        os.system(statement)
        xml_path = data_path+"out.xml"
        MyDict = parsexml(xml_path)
        for item in get_Heading(MyDict): 
        	query = "INSERT INTO slide_vs_heading VALUES (\""+ slide_sha +"\",\"" + item + "\")"
        	cur.execute(query)
        	db.commit()

        '''	
        for item in get_text_body(MyDict): 
        	query = "INSERT INTO slide_vs_body VALUES (\""+ slide_sha +"\",\"" + item + "\")"
        	cur.execute(query)
        	db.commit()

        '''
        
        for item in get_meta_information(MyDict): 
        	query = "INSERT INTO slide_vs_meta VALUES (\""+ slide_sha +"\",\"" + item + "\")"
        	cur.execute(query)
        	db.commit()


    #UNCOMMENT IT
    os.system("rm -rf " + data_path + "out.xml")
    os.system("rm -rf" + directory)

    if  is_pdf == False:
        command = "rm " + "../" + file_core + ".pdf"
        os.system(command)


    os.system("rm " + "../upload/" + sha1_file + ".pdf")

else: 
    for x in cur.fetchall():
        print "Old Doc"
        pathname = x[0]
        curr=db.cursor()
        cur.execute ("""UPDATE upload_files SET file_link=%s WHERE file_link=%s""",(pathname,filepath))
        db.commit()            
        os.system("rm -rf ../"+filepath)
        os.system("rm -rf ../"+file_core+".pdf")
 

cur.commit()

# if len(cur.fetchall()) == 0:
#     print "New document found"
# else:
#     print "Old document"

