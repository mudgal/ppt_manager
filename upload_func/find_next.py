import sys
import MySQLdb
import os
import operator
import re

import commands

db = MySQLdb.connect(host="localhost",user="root",passwd="root", db="secure_login")

stops = set(['all', 'just', 'being', 'over', 'both', 'through', 'yourselves', 'its', 'before', 'herself', 'had', 'should', 'to', 'only', 'under', 'ours', 'has', 'do', 'them', 'his', 'very', 'they', 'not', 'during', 'now', 'him', 'nor', 'did', 'this', 'she', 'each', 'further', 'where', 'few', 'because', 'doing', 'some', 'are', 'our', 'ourselves', 'out', 'what', 'for', 'while', 'does', 'above', 'between', 't', 'be', 'we', 'who', 'were', 'here', 'hers', 'by', 'on', 'about', 'of', 'against', 's', 'or', 'own', 'into', 'yourself', 'down', 'your', 'from', 'her', 'their', 'there', 'been', 'whom', 'too', 'themselves', 'was', 'until', 'more', 'himself', 'that', 'but', 'don', 'with', 'than', 'those', 'he', 'me', 'myself', 'these', 'up', 'will', 'below', 'can', 'theirs', 'my', 'and', 'then', 'is', 'am', 'it', 'an', 'as', 'itself', 'at', 'have', 'in', 'any', 'if', 'again', 'no', 'when', 'same', 'how', 'other', 'which', 'you', 'after', 'most', 'such', 'why', 'a', 'off', 'i', 'yours', 'so', 'the', 'having', 'once'])



def get_list(string):
	global stops
	word_list = []
	for item in (string.lower()).split():
		if item not in stops:
			word_list.append(item)

	return word_list

'''
def findslide_body(search_str):
	global db 
	slide_list = []

	for word in search_str.lower().split():
		cur = db.cursor()
		q= "SELECT * from slide_vs_body where body like \""+ word + "\""
		cur.execute(q)
		#Take result of q in slide_sha1
		#query = "INSERT INTO count_heading VALUES (\""+ slide_sha1+"\")"    
		#append slide_sha1 into word_dict      
		results = cur.fetchall()
		for row in results:
			slide_list.append(row[0])
			
	#slide_list is a multi-list
	x = {i:slide_list.count(i) for i in set(slide_list)}

	sorted_x = sorted(x.items(), key=operator.itemgetter(1))
	sorted_x.reverse()

	file_list = []

	for item in sorted_x:
		file_list.append(item[0] + ".pdf")

	return file_list
'''

def find_prev(item):
	query = "select * from doc_vs_slide where slide_sha1 like \"" + item.split(".pdf")[0] + "\""
	global db
	cur = db.cursor()
	my_list=[]
	cur.execute(query)
	results = cur.fetchall()
	for row in results:
		doc_name = row[0]
		slide_no = int(row[2])
		break

	slide_new = slide_no - 1

	if (slide_new == 0):
		my_list.append("start.pdf")
		return my_list

	query = "select * from doc_vs_slide where slide_no = " + str(slide_new) + " and doc_sha1 like \"" + doc_name + "\"" 
	cur.execute(query)
	results = cur.fetchall()

	for row in results:
		my_list.append(row[1] + ".pdf")
		return my_list


def find_next(item):
	query = "select * from doc_vs_slide where slide_sha1 like \"" + item.split(".pdf")[0] + "\""
	global db
	cur = db.cursor()
	my_list=[]
	cur.execute(query)
	results = cur.fetchall()
	for row in results:
		doc_name = row[0]
		slide_no = int(row[2])
		break

	slide_new = slide_no + 1

	query = "select * from doc_vs_slide where slide_no = " + str(slide_new) + " and doc_sha1 like \"" + doc_name + "\"" 
	cur.execute(query)
	results = cur.fetchall()
	
	for row in results:
		my_list.append(row[1] + ".pdf")
		return my_list

	my_list.append("end.pdf")
	return my_list

string = sys.argv[1]

print str(find_next(string))
