use secure_login;

DROP TABLE IF EXISTS `upload_files`;

CREATE TABLE IF NOT EXISTS `upload_files` 
(
  `file` varchar(200) NOT NULL,
  `file_link` varchar(300) NOT NULL,
  `owner` int NOT NULL,
  `my_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   primary key(`file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
