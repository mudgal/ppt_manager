<?php

	error_reporting(E_ALL);
	
	$comefrom = $_SERVER['HTTP_REFERER'];
	if ( $comefrom != "http://localhost/doodle/upload.php" )
	{
		echo "<script>alert('Access Denied')</script>";
		exit(0);
	}

	$hard_disk_upload_directory = "/var/www/doodle/upload/";  
		
	$status_file1 = validate_and_upload("file1");
	
	function validate_and_upload($input_tag_name)
	{
		$allowedExts = array("gif", "jpeg", "jpg", "png", "ppt", "pptx", "doc", "pdf", "xls", "xlxs", "xlsm", "txt", "docx", "zip", "rar", "tar.gz", "php", "out");
		//$temp = explode(".", $_FILES["project_file1"]["name"]); //not that efficient, the file name may itself contain "."
		//$extension = end($temp);
		$filename = $_FILES[$input_tag_name]['name'];
		if ( !$filename )
			return 0;
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		
		if ( ($_FILES[$input_tag_name]["size"] < 67108864) && in_array($extension, $allowedExts) ) // 67108864 is 64.00 MB
		{
			if ($_FILES[$input_tag_name]["error"] > 0) 
			{
				echo "Return Code: " . $_FILES[$input_tag_name]["error"] . "<br>";
				return -1; // error while uploading
			} 
			else 
			{
			//	echo "Upload: " . $_FILES[$input_tag_name]["name"] . "<br>";
			//	echo "Type: " . $_FILES[$input_tag_name]["type"] . "<br>";
			//	echo "Size: " . ($_FILES[$input_tag_name]["size"] / 1024) . " kB<br>";
			//	echo "Temp file: " . $_FILES[$input_tag_name]["tmp_name"] . "<br>";
				if (file_exists("/var/www/doodle/upload/" . $_FILES[$input_tag_name]["name"])) 
				{
					echo $_FILES[$input_tag_name]["name"] . " already exists. ";
					$hard_disk_upload_directory = "/var/www/doodle/upload/";

					

					//$path_parts = pathinfo($_FILES[$input_tag_name]["name"]);
					//$upload_path = $path_parts['filename'].'_'.time().'.'.$path_parts['extension']



					//move_uploaded_file($_FILES[$input_tag_name]["tmp_name"], $hard_disk_upload_directory . $_FILES[$input_tag_name]["name"]) ;
					return 2;
				} 
				else 
				{
					$hard_disk_upload_directory = "/var/www/doodle/upload/";
					if (move_uploaded_file($_FILES[$input_tag_name]["tmp_name"], $hard_disk_upload_directory . $_FILES[$input_tag_name]["name"]) )
						return 1;
					else
						return -1;
				}
			}
		} 
		else 
		{
			echo "Invalid file";
			return -1;
		}
	}
	
	define('DB_SERVER', 'localhost');
	define('DB_USERNAME', 'root');
	define('DB_PASSWORD', 'root');
	define('DB_DATABASE', 'secure_login');
	define('DB_TABLE', 'upload_files');
	
	// note that here, on form action = "abc.php", $_POST[] gets variables by name and not by id !!!
	
	$project_file1 = $_FILES["file1"]['name'];
	$upload_directory = "../localhost/doodle/upload";
	
	if ( !$project_file1 )
	{
		$project_file1 = "";
		$project_file1_full_path = "";
	}
	else
	{
		$project_file1_full_path = $upload_directory.$project_file1;
	}

	
	function fill_null_if_empty($my_var)
	{
		if ( !$my_var || $my_var == "" || $my_var == NULL || $my_var == "null" || $my_var == "NULL" )
			return "";
		else
			return $my_var;
	}
?>
