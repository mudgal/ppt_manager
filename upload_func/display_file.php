<?php

	$flag = 0;
	if ( $_SERVER['HTTP_REFERER'] != "https://172.27.22.218/doodle/protected_page.php")
	{
		$flag = $flag+1;
		//header( "Location: ../403.php" );
		//exit(1);
	}
	if ( $_SERVER['HTTP_REFERER'] != "https://172.27.22.218/doodle/file_handler.php")
	{
		$flag = $flag+1;
		//header( "Location: ../403.php" );
		//exit(1);
	}
	if($flag != 1)	// either one of above will increase flag
	{
		header( "Location: ../403.php" );
		exit(1);
	}


//error_reporting(E_ALL); // or E_STRICT
//	ini_set("display_errors",1);
//	ini_set("memory_limit","1024M");
//ini_set('display_errors', 'on');
error_reporting(0);

	include_once '../includes/db_connect.php';
	include_once '../includes/functions.php';
	//include once '../includes/psl-config.php';
	
	define('DB_TABLE', 'upload_files');
	sec_session_start(); 
	$user_id = htmlentities($_SESSION['user_id']);
	$user_name = htmlentities($_SESSION['username']);
	$project_file1 = $_GET['file_name'];
	if(login_check($mysqli) == true) 
	{
		//echo $user_name;
		//echo $project_file1;
		// Need to see if the logged in person is really the owner of the file. Then only will the file be displayed
		$db2 = new PDO('mysql:host=' .HOST . ';dbname=' .DATABASE . ';charset=utf8', USER, PASSWORD);
		//$db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db2->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		
		$query_owner_check = "SELECT owner from " . DB_TABLE . " where file = (:fid)";
		$parameters2 = array(':fid'=>$project_file1);
		$statement1 = $db2->prepare($query_owner_check);
		if ($statement1->execute($parameters2) )
		{
			while ($row = $statement1->fetch(PDO::FETCH_ASSOC)) 
			{
				//echo "owner " . $row['owner'] . "<br>logged in user ";
				//echo $user_id;
				if ( $row['owner'] == $user_id )
				{
					// I am the true owner, so chill!
					//echo "<script>alert('accessing other's file is denied');</script>";
				}
				else
				{
					//echo "<script>alert('accessing other's file is denied');</script>";
					header( "Location: ../403.php" );
					exit(1);
				}
			}
		}
		else
		{
			echo "MySQL query failed<br>";
			header( "Location: ../protected_page.php" );
			exit(1);
		}
	} 
	else 
	{ 
		//echo 'You are not authorized to access this page, please login.';
		header( "Location: ../403.php" );
		exit(1);
	}




//ignore_user_abort(true);
//set_time_limit(0); // disable the time limit for this script

$path = "/var/doodle/upload/"; // change the path to fit your websites document structure
$dl_file = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $_GET['file_name']); // simple file name validation

$fullPath = $path.$dl_file;

//echo $fullPath;

//if ( is_readable($fullPath) )
//	echo "file readable";
//else
//	echo "ERROR";


if ($fd = fopen ($fullPath, "r")) {
    $fsize = filesize($fullPath);
    $path_parts = pathinfo($fullPath);
    $ext = strtolower($path_parts["extension"]);


switch( $ext )
{
  case "pdf": $ctype="application/pdf"; break;
  case "exe": $ctype="application/octet-stream"; break;
  case "zip": $ctype="application/zip"; break;
  case "doc": $ctype="application/msword"; break;
  case "xls": $ctype="application/vnd.ms-excel"; break;
  case "ppt": $ctype="application/ppt"; break;
  case "pptx": $ctype="application/pptx"; break;
  case "gif": $ctype="image/gif"; break;
  case "png": $ctype="image/png"; break;
  case "jpeg":
  case "jpg": $ctype="image/jpg"; break;
  default: $ctype="application/force-download";
}

header('Content-Description: File Transfer');
header("Content-Type: $ctype");
header('Content-Disposition: filename="'.basename($fullPath).'"'); //<<< Note the " " surrounding the file name
header('Content-Transfer-Encoding: binary');
header('Connection: Keep-Alive');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($fullPath));
readfile($fullPath);
}
fclose ($fd);


?>
