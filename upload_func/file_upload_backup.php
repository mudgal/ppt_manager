<?php

	/*error_reporting(E_ALL); // or E_STRICT
	ini_set("display_errors",1);
	ini_set("memory_limit","1024M");*/
	error_reporting(0);

	include_once '../includes/db_connect.php';
	include_once '../includes/functions.php';

	// Include database connection and functions here.  See 3.1. 
	sec_session_start(); 
	$user_id = htmlentities($_SESSION['user_id']);
	$user_name = htmlentities($_SESSION['username']);
	if(login_check($mysqli) == true) 
	{
		// Add your protected page content here!
	} 
	else 
	{ 
		//echo 'You are not authorized to access this page, please login.';
		header( "Location: ../403.php" );
		exit(1);
	}


	$project_file1 = $_FILES['file1']['name'];
	$project_file1 = $user_name . '_' . $project_file1;


	//echo "dfg $project_file1<br>";

	$upload_directory = "upload/";

	if ( !$project_file1 )
	{
		$project_file1 = "";
		$project_file1_full_path = "";
	}
	else
	{
		$project_file1_full_path = $upload_directory.$project_file1;
	}

	$status_file1 = validate_and_upload("file1",$project_file1_full_path, $user_name, $upload_directory);

	if ( $status_file1 == 2 ) 
	{
		$project_file1 = $_FILES['file1']['name'];
		$extension = pathinfo($project_file1, PATHINFO_EXTENSION);
		$path_parts = pathinfo($project_file1);
		$project_file1 = $user_name . '_' . $path_parts['filename']."_".time();

		if ($extension)
			$project_file1 .= '.' . $extension;
		$project_file1_full_path = $upload_directory.$project_file1;

		echo "file name for mysql  is $project_file1<br><br>";
	}

	function validate_and_upload($input_tag_name,$project_file1_full_path, $user_name,$upload_directory)
	{
		$allowedExts = array("gif", "jpeg", "jpg", "png", "ppt", "pptx", "doc", "pdf", "xls", "xlxs", "xlsm", "txt", "docx", "zip", "rar", "tar.gz");
		//$temp = explode(".", $_FILES["project_file1"]["name"]); //not that efficient, the file name may itself contain "."
		//$extension = end($temp);
		$filename = $_FILES[$input_tag_name]['name'];
		if ( !$filename )
			return 0;
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		
		if ( ($_FILES[$input_tag_name]["size"] < 67108864) && in_array($extension, $allowedExts) ) // 67108864 is 64.00 MB
		{
			if ($_FILES[$input_tag_name]["error"] > 0) 
			{
				echo "Return Code: " . $_FILES[$input_tag_name]["error"] . "<br>";
				return -1; // error while uploading
			} 
			else 
			{
			//	echo "Upload: " . $_FILES[$input_tag_name]["name"] . "<br>";
			//	echo "Type: " . $_FILES[$input_tag_name]["type"] . "<br>";
			//	echo "Size: " . ($_FILES[$input_tag_name]["size"] / 1024) . " kB<br>";
			//	echo "Temp file: " . $_FILES[$input_tag_name]["tmp_name"] . "<br>";

				if (file_exists("/var/www/doodle/".$project_file1_full_path)) 
				{
					$project_file1 = $_FILES['file1']['name'];
					echo "File $project_file1 already exists<br>";
					$extension = pathinfo($project_file1, PATHINFO_EXTENSION);
					$path_parts = pathinfo($project_file1);
					$project_file1 = $user_name . '_' . $path_parts['filename']."_".time();

					if ($extension)
						$project_file1 .= '.' . $extension;

					$project_file1_full_path = $upload_directory.$project_file1;
					move_uploaded_file($_FILES[$input_tag_name]["tmp_name"], "/var/www/doodle/".$project_file1_full_path);
					return 2;
				} 
				else 
				{
					if  ( move_uploaded_file($_FILES[$input_tag_name]["tmp_name"], "/var/www/doodle/".$project_file1_full_path) )  
					{
						return 1;
					}
					else
					{
						echo "File upload failed<br>";
						return -1;
					}
				}
			}
		} 
		else 
		{
			echo "Invalid file";
			return -1;
		}
	}

	define('DB_TABLE', 'upload_files');
	
	// note that here, on form action = "abc.php", $_POST[] gets variables by name and not by id !!!
	

	
	try 
	{
		$db2 = new PDO('mysql:host=' .HOST . ';dbname=' .DATABASE . ';charset=utf8', USER, PASSWORD);
		$query_duplicate_check = "SELECT file from" . DB_TABLE . " where file = (:fid)";
		$parameters2 = array(':fid'=>$project_file1);
		$statement1 = $db2->prepare($query_duplicate_check);
		$statement1->execute($parameters2);
		if ( $statement1->rowCount() > 0 )
		{
			echo "<script>alert('File name already exists. Try another name');</script>";
			exit(0);
		}
		else
		{
			//echo "print something<br>";
		}
		
		
		$db = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE . ';charset=utf8', USER, PASSWORD);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$query = "INSERT into " . DB_TABLE . "( file, file_link, owner ) values ( (:fid), (:flink), (:uid))";

		$parameters1 = array(':fid'=>$project_file1,':flink'=>$project_file1_full_path, ':uid'=>$user_id);
		$statement1 = $db->prepare($query);
		$statement1->execute($parameters1);
		echo "$project_file1 file has been successfully uploaded!<br>";
		echo "<br><br><br><br><br>Redirecting................................................:):P<br>";
	}
	catch(Exception $e) 
	{
		echo 'Exception -> ';
		var_dump($e->getMessage());
	}

	
	header( "refresh:4; url=../protected_page.php" );
	

?>
