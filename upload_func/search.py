import sys
import MySQLdb
import os
import operator
import re

import json
import commands

db = MySQLdb.connect(host="localhost",user="root",passwd="root", db="secure_login")

stops = set(['all', 'just', 'being', 'over', 'both', 'through', 'yourselves', 'its', 'before', 'herself', 'had', 'should', 'to', 'only', 'under', 'ours', 'has', 'do', 'them', 'his', 'very', 'they', 'not', 'during', 'now', 'him', 'nor', 'did', 'this', 'she', 'each', 'further', 'where', 'few', 'because', 'doing', 'some', 'are', 'our', 'ourselves', 'out', 'what', 'for', 'while', 'does', 'above', 'between', 't', 'be', 'we', 'who', 'were', 'here', 'hers', 'by', 'on', 'about', 'of', 'against', 's', 'or', 'own', 'into', 'yourself', 'down', 'your', 'from', 'her', 'their', 'there', 'been', 'whom', 'too', 'themselves', 'was', 'until', 'more', 'himself', 'that', 'but', 'don', 'with', 'than', 'those', 'he', 'me', 'myself', 'these', 'up', 'will', 'below', 'can', 'theirs', 'my', 'and', 'then', 'is', 'am', 'it', 'an', 'as', 'itself', 'at', 'have', 'in', 'any', 'if', 'again', 'no', 'when', 'same', 'how', 'other', 'which', 'you', 'after', 'most', 'such', 'why', 'a', 'off', 'i', 'yours', 'so', 'the', 'having', 'once'])



# Simple Search functions

def findslide_heading(search_str):
	global db 
	global stops

	slide_list = []

	my_list = search_str.lower().split()

	data_list = []

	for item in my_list:
		if item not in stops:
			data_list.append(item)

	n = len(data_list)

	for word in data_list:
		cur = db.cursor()
		q= "SELECT * from slide_vs_heading where heading like \""+ word + "\""
		cur.execute(q)
		#Take result of q in slide_sha1
		#query = "INSERT INTO count_heading VALUES (\""+ slide_sha1+"\")"    
		#append slide_sha1 into word_dict      
		results = cur.fetchall()
		for row in results:
			slide_list.append(row[0])
			
	#slide_list is a multi-list
	x = {i:slide_list.count(i) for i in set(slide_list)}

	sorted_x = sorted(x.items(), key=operator.itemgetter(1))
	sorted_x.reverse()

	file_list1 = []
	file_list2 = []

	for item in sorted_x:
		if ( n == 1):
			file_list1.append(item[0] + ".pdf")
		else: 
			if (int(item[1]) > 1):
				file_list1.append(item[0] + ".pdf")
			else :
				file_list2.append(item[0] + ".pdf")

	return {"major" : file_list1 , "minor" : file_list2}


def pdfsearch(regex_string):
	command_output = commands.getstatusoutput('find ../upload/data/  -name \'*.pdf\' -exec pdfgrep -H -i \'' + regex_string + '\' {} \;')

	file_list = []
	for item in command_output[1].split('\n'):
		if ".pdf" in item: 
			file_list.append((item.split(':')[0]).split('/')[-1])
	return file_list


def get_list(string):
	global stops
	word_list = []
	for item in (string.lower()).split():
		if item not in stops:
			word_list.append(item)

	return word_list

def findslide_body(search_str):
	global db 
	slide_list = []

	for word in search_str.lower().split():
		cur = db.cursor()
		q= "SELECT * from slide_vs_body where body like \""+ word + "\""
		cur.execute(q)
		#Take result of q in slide_sha1
		#query = "INSERT INTO count_heading VALUES (\""+ slide_sha1+"\")"    
		#append slide_sha1 into word_dict      
		results = cur.fetchall()
		for row in results:
			slide_list.append(row[0])
			
	#slide_list is a multi-list
	x = {i:slide_list.count(i) for i in set(slide_list)}

	sorted_x = sorted(x.items(), key=operator.itemgetter(1))
	sorted_x.reverse()

	file_list = []

	for item in sorted_x:
		file_list.append(item[0] + ".pdf")

	return file_list

'''
def find_prev(item):
	query = "select * from doc_vs_slide where slide_sha1 like \"" + item.split(".pdf")[0] + "\""
	global db
	cur = db.cursor()

	cur.execute(query)
	results = cur.fetchall()
	for row in results:
		doc_name = row[0]
		slide_no = int(row[2])
		break

	slide_new = slide_no - 1

	if (slide_new == 0):
		return "start.pdf"

	query = "select * from doc_vs_slide where slide_no = " + str(slide_new) + " and doc_sha1 like \"" + doc_name + "\"" 
	cur.execute(query)
	results = cur.fetchall()

	for row in results:
		return row[1] + ".pdf"


def find_next(item):
	query = "select * from doc_vs_slide where slide_sha1 like \"" + item.split(".pdf")[0] + "\""
	global db
	cur = db.cursor()

	cur.execute(query)
	results = cur.fetchall()
	for row in results:
		doc_name = row[0]
		slide_no = int(row[2])
		break

	slide_new = slide_no + 1

	query = "select * from doc_vs_slide where slide_no = " + str(slide_new) + " and doc_sha1 like \"" + doc_name + "\"" 
	cur.execute(query)
	results = cur.fetchall()
	
	for row in results:
		return row[1] + ".pdf"

	return "end.pdf"

'''
string = sys.argv[1]
head_dict = findslide_heading(string)
file_list1 = head_dict["major"]
file_list2 = head_dict["minor"]
file_list3 = pdfsearch('.*'.join(get_list(string)))
file_list4 = pdfsearch('|'.join(get_list(string)))

file_list = (file_list1 + file_list3[0:10] + file_list2 + file_list4[0:10])


my_list = []

for item in file_list:
	if item not in my_list:
		my_list.append(item)

print str(my_list)

