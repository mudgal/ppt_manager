<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="styles/ssg.css">
    <head>
       <meta charset="utf-8">
    	<meta content="Official website of Slide selector" name="description">
    	<title>Slide Selector</title>
    	<link href="styles/ssg.css" rel="stylesheet">
	<script type="text/JavaScript" src="js/sha512.js"></script> 
    	<script type="text/JavaScript" src="js/forms.js"></script>
    	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    	<link href='http://fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="styles/main.css" />
    </head>
    <body >
	<div id="header">
    <div id="header-content">&nbsp &nbsp &nbsp  Slide Selector    </div> </div>
    <div class="clear"></div>
    <div id="menu">
    <div id="menu-content">
        <div id="left-menu">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="register.php">Register</a></li>
            <li><a href="about.html">About Us</a></li>
            <li><a href="contact.html">Contact Us</a></li>    
             
        </div>
	<div id="right-menu">
		<ul> <li><a href="includes/logout.php"><b>Logout</b></a></li> </ul>
	</div>
    </div>
    </div>
<div class="col-md-offset-1 col-md-6" style="margin-top:14%">
        <?php if (login_check($mysqli) == true) : ?>
        <p class="bg-success" style="width:120%"><font color="red" size=6 style="margin-left:28% "><b>&nbsp &nbsp &nbsp WELCOME <?php echo htmlentities($_SESSION['username']); ?>!</b></font></p>
            <p style="margin-left:1%">
                This is your personal protected page.  
            </p>
            
	    <p style="margin-left:1%"> Some links </p>
	    <p style="margin-left:1%">Return to <a href="login_here.php">login page</a></p>
    		
		<p style="margin-left:1%">Search required slides <a href="search.php">here</a></p>
	    <p style="margin-left:1%"><a href="file_handler.php"><font color="blue">File Handler and Manager</font></a></p>
	    </div>
	    <div class="col-md-offset-0 col-md-6 well" style="margin-left:12%">
	    <p>My uploaded files</p>
	    <table style="width:130%">
	    <tr>
	    <td>Filename</td>
	    <td>Link</td>
	    <td>UploadTime</td></tr>
	    

	    <?php
	
		define('DB_SERVER', 'localhost');
		define('DB_USERNAME', 'root');
		define('DB_PASSWORD', 'root');
		define('DB_DATABASE', 'secure_login');
		define('DB_TABLE', 'upload_files');
	
		$user_id = htmlentities($_SESSION['user_id']);
		$db2 = new PDO('mysql:host=' .DB_SERVER . ';dbname=' . DB_DATABASE . ';charset=utf8', DB_USERNAME,  DB_PASSWORD);
		$query = "SELECT file, file_link, my_timestamp from " . DB_TABLE . " where owner = (:uid)";
		$parameters2 = array(':uid'=>$user_id);
		$statement1 = $db2->prepare($query);		
		if ( $statement1->execute($parameters2) )
		{
			
			while ($row = $statement1->fetch(PDO::FETCH_ASSOC)) 
			{
				
				echo "<tr><td>".$row['file']."</td>";
				echo "<td><a href='" . $row['file_link'] . "'>" . "Link " . "</a></td>";
				echo "<td>".$row['my_timestamp']."</td></tr>";
				//echo "<br>";
			}
		}
		else
		{
			//echo "print something<br>";
		}

	?>
    </table>
    </div>
   

        <?php else : header( "Location: 403.php" ); ?>
            <p>
                <span class="error">You are not authorized to access this page.</span> Please <a href="login_here.php">login</a>.
            </p>
        <?php endif; ?>






    </body>
</html>
