<?php

	error_reporting(0);


include_once 'includes/db_connect.php';
include_once 'includes/reset2.inc.php';
include_once 'includes/psl-config.php';
include_once 'includes/functions.php';

session_start(); // to prevent re-submitting forms by clicking on back button

if( isset($_GET['token']) )
{
	$token = $_GET['token'];
	$db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE .';charset=utf8', USER, PASSWORD);

    $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $query1 = "SELECT email from reset_attempts WHERE token = (:to)";
	$query11 = "SELECT used_status from reset_attempts WHERE token = (:to)";
	$query12 = "SELECT time from reset_attempts WHERE token = (:to)";
	$query2 = "UPDATE reset_attempts SET used_status = 1 WHERE token = (:to)";

    $statement1 = $db2->prepare($query1);
	$statement11 = $db2->prepare($query11);
	$statement12 = $db2->prepare($query12);
	$statement2 = $db2->prepare($query2);

    $params1 = array(':to'=>$token);
	$params11 = array(':to'=>$token);
	$params12 = array(':to'=>$token);
	$params2 = array(':to'=>$token);

    if ($statement1->execute($params1))
	{
		while ($row = $statement1->fetch(PDO::FETCH_ASSOC))
		{
			 $email = $row['email']; 
		}
		
		if ($statement12->execute($params12))
		{
			while ($row = $statement12->fetch(PDO::FETCH_ASSOC))
			{
				$now = time();
				$db_time = $row['time']; 
				if ( $now - $db_time > (60*60) )
				{
					// expired link due to time expire
					header('Location: error.php?err=The reset link has been expired due to timelapse');
    				exit(1);
				}
			}
		}
		else
		{
			header('Location: error.php?err=Connection Failed');
    			exit(1);
		}


		if ($statement11->execute($params11))
		{
			while ($row = $statement11->fetch(PDO::FETCH_ASSOC))
			{
				$used_status = $row['used_status']; 
				if ( $used_status > 0 )
				{
					// expired link due to multi use
					header('Location: error.php?err=The reset link has been used and is expired');
    				exit(1);
				}
			}
		}
		else
		{
			header('Location: error.php?err=Connection Failed');
    		exit(1);
		}

		if ($statement2->execute($params2))
		{
			// do nothing as it was update statement
		}
		else
		{
			header('Location: ../error.php?err=Connection Failed');
    		exit(1);
		}
	}
	else
	{
		echo "<script>alert('Access Denied');</script>";
		exit(0);
	}
	session_destroy();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Password Reset for Forgot Password</title>
        <link rel="stylesheet" href="styles/main.css" />
		<script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
    </head>
    <body>
	    <form method="post" name="forgot_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
		Your E-mail:<input type="text" name="email" id="email" value="<?php echo htmlentities($email); ?>" readonly>
		Please Enter Activation Code (from email)<input type="text" name="temp_pass" id="temp_pass"><br><br>
		<input type="hidden" name="token" id="token" value ="<?php echo htmlentities($token); ?>" readonly>
            New Password: <input type="password" name="password" id="password"/><br>
            Confirm password: <input type="password" name="confirmpwd" id="confirmpwd" /><br>
            <input type="button" value="Change" onclick="return resetforgotformhash(this.form,
                                   this.form.email,
									this.form.token,
									this.form.temp_pass,
                                   this.form.password,
                                   this.form.confirmpwd);" /> 
        </form>
		
    </body>
</html>
      
