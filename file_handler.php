<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="styles/ssg.css">
<?php
	include_once 'includes/db_connect.php';
	include_once 'includes/functions.php';

	// Include database connection and functions here.  See 3.1. 
	sec_session_start(); 
?>
<html>
 <head>
       <meta charset="utf-8">
    	<meta content="Official website of Slide selector" name="description">
    	<title>Slide Selector</title>
    	<link href="styles/ssg.css" rel="stylesheet">
	<script type="text/JavaScript" src="js/sha512.js"></script> 
    	<script type="text/JavaScript" src="js/forms.js"></script>
    	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    	<link href='http://fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="styles/main.css" />
    </head>
<body>
<div id="header">
    <div id="header-content">&nbsp &nbsp &nbsp  Slide Selector    </div> </div>
    <div class="clear"></div>
    <div id="menu">
    <div id="menu-content">
        <div id="left-menu">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="register.php">Register</a></li>
            <li><a href="about.html">About Us</a></li>
            <li><a href="contact.html">Contact Us</a></li>    
             
        </div>
	<div id="right-menu">
		<ul> <li><a href="includes/logout.php"><b>Logout</b></a></li> </ul>
	</div>
    </div>
    </div>
<div class="col-md-offset-1 col-md-6" style="margin-top:10%">
	<?php if (login_check($mysqli) == true) : ?>
        <p class="bg-success" style="width:120%"><font color="red" size=6 style="margin-left:28%"><b>&nbsp &nbsp &nbsp WELCOME <?php echo htmlentities($_SESSION['username']); ?>!</b></font></p>
            <p style="margin-left:1%">
                This is your file handler page. You can browse through your uploaded files.
            </p>
            
	    <p style="margin-left:1%"> Some links </p>
	    
        <?php else : 
            
	//echo '<p><span class="error">You are not authorized to access this page.</span>';// Please <a href="login_here.php">login</a>.</p>';
	header( "Location: 403.php" );
	exit(0);
	?>
        <?php endif; ?>


<div style="margin-left:1%">
<form action="upload_func/file_upload.php" method="post" enctype="multipart/form-data">
<label for="file1">Filename:</label>
<input type="file" class="btn btn-primary" name="upload[]" id="file1" multiple="multiple" style="width:26%"><br>
<input type="submit" class="btn btn-primary" name="submit" value="Submit">
</form>

<div class="col-md-offset-0 col-md-14 well" style="margin-top:1%">
<p>Here are your files:</p>
<table style="width:130%">
	    <tr>
	    <td>Filename</td>
	    <td>Link</td>
	    <td>UploadTime</td></tr>
	    <?php
	
		define('DB_SERVER', 'localhost');
		define('DB_USERNAME', 'root');
		define('DB_PASSWORD', 'root');
		define('DB_DATABASE', 'secure_login');
		define('DB_TABLE', 'upload_files');
	
		$user_id = htmlentities($_SESSION['user_id']);
		$db2 = new PDO('mysql:host=' .DB_SERVER . ';dbname=' . DB_DATABASE . ';charset=utf8', DB_USERNAME,  
			DB_PASSWORD);
		$query = "SELECT file, file_link, my_timestamp from " . DB_TABLE . " where owner = (:uid)";
		$parameters2 = array(':uid'=>$user_id);
		$statement1 = $db2->prepare($query);		
		if ( $statement1->execute($parameters2) )
		{
			while ($row = $statement1->fetch(PDO::FETCH_ASSOC)) 
			{

				echo "<tr><td>".$row['file']."</td>";
				echo "<td><a href='" . $row['file_link'] . "'>" . "Link " . "</a></td>";
				echo "<td>".$row['my_timestamp']."</td></tr>";
			}
		}
		else
		{}

	?>
</table>
<br><br>
<p>Return to <a href="login_here.php">Login page</a></p>
<p>Go to <a href="protected_page.php">My Own Page</a></p>
</div>
</div>
</div>
</body>
</html> 

