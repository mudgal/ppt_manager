<?php

/* 
 * Copyright (C) 2013 peter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once 'db_connect.php';
include_once 'psl-config.php';
include_once 'functions.php';

$error_msg = "";

sec_session_start();

if (isset($_POST['username'],$_POST['p'])) 
{
    // Sanitize and validate the data passed in

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    
    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
    if (strlen($password) != 128) 
	{
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Invalid password configuration.</p>';
    }

    if ( $username != $_SESSION['username'] )	// somebody edited the username in the previous form
    {
	header('Location: ./error.php?err=Form in the previous page was manipulated');
	exit(1);
   }

    if (empty($error_msg)) 
	{
        // Create a random salt
        $random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
        // Create salted password 
        $password = hash('sha512', $password . $random_salt);
		try
		{


			$db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE . ';charset=utf8', USER, PASSWORD);

			$db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);	

			$query1 = "UPDATE members set password = (:pass), salt = (:salt) where username = (:usern)";


			$statement1 = $db2->prepare($query1);	
			$params =  array(':pass'=>$password,':salt'=>$random_salt,':usern'=>$username);
	 
		    if (! $statement1->execute($params)) 
			{
		        header('Location: ./error.php?err=Reset password failure: UPDATE');
		        exit(1);
		    }
		    else
			{
				header('Location: ./reset_success.php');
				exit();
			}
		}
		catch(Exception $e) 
		{
			echo "<script>alert('dfgdfghdfhfh');</script>";
		    echo $e->getMessage();
			header('Location: ./error.php?err=Reset password failure: MySQL connection not established');
		}
    }
	else
	{
		echo "<script>alert('error')</script>";
	}
}
?>
