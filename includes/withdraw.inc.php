<?php

include_once '../includes/db_connect.php';
include_once '../includes/psl-config.php';
include_once '../includes/functions.php';

sec_session_start();


if ( isset($_POST['withamt'],$_POST['balance'],$_POST['username']) )
{
    $username=$_POST['username'];
	$balance = $_POST['balance'];
	$withamt = $_POST['withamt'];
echo "1".$username;
	if (!is_numeric($balance) || $balance < 1 || $balance != round($balance))
	{
		echo "<script>alert('Your Balance is not an integer');</script>";
		exit(0);
	}
	if (!is_numeric($withamt) || $withamt < 1 || $withamt != round($withamt))
	{
		echo "<script>alert('Withdraw amount is not a non-negative integer');</script>";
		exit(0);
	}
	
	if ( $withamt > $balance )
	{
		header("Location: ../error.php?err=Cannot withdraw due to insufficient balance");
        exit(0);
	}

    
        try
        {
            $db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE .';charset=utf8', USER, PASSWORD);

            $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

           
			$query3 = "UPDATE members SET balance = balance - (:new_bal) where username = (:user)";

            
			$statement3 = $db2->prepare($query3);

            
			$params3 = array(':new_bal'=>$withamt,':user'=>$username);

            

			if (! $statement3->execute($params3))
            {
                    header('Location: ../error.php?err=Withdraw failure:UPDATE');
                    exit(1);
            }
            else
            {
                    header('Location: ../transfer_success.php');
                    exit(0);
            }
		}
		catch( Exception $e)
		{
		        echo $e->getMessage();
		        header('Location: ../error.php?err=MySQL Connection failure');
		        exit();
		}
    }
else
{
echo "2".$username;
}
?>
