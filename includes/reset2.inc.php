<?php

/* 
 * Copyright (C) 2013 peter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* FORGOT PASSWORD */

	error_reporting(0);

include_once 'db_connect.php';
include_once 'psl-config.php';

$error_msg = "";



if (isset($_POST['email'],$_POST['p'],$_POST['temp_pass'],$_POST['token'])) 
{
    // Sanitize and validate the data passed in
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
    $temp_pass = filter_input(INPUT_POST, 'temp_pass', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
	$token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

	$db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE . ';charset=utf8', USER, PASSWORD);

	$db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);	

	$query2 = "SELECT temp_pass from reset_attempts where email = (:email) AND token = (:token)";
	$query_email_verify = "SELECT email from reset_attempts where token = (:token)";

	$statement2 = $db2->prepare($query2);	
	$statement_email_verify = $db2->prepare($query_email_verify);
	$params_email_verify =  array(':token'=>$token);

	$params2 =  array(':token'=>$token,':email'=>$email);


	if ($statement_email_verify->execute($params_email_verify))
	{
		while ($row = $statement_email_verify->fetch(PDO::FETCH_ASSOC))
		{ 
			$db_email = $row['email']; 
		}
		if ( $db_email != $email )
		{
			header('Location: ./error.php?err=Email was manupulated in the previous form');
	        	exit(1);
		}
	}
	else
	{
		header('Location: ./error.php?err=Connection failed');
	        exit(1);
	}

    	if ($statement2->execute($params2))
	{
		while ($row = $statement2->fetch(PDO::FETCH_ASSOC))
		{ 
			$db_temp_pass = $row['temp_pass']; 
		}
		if ( $db_temp_pass != $temp_pass )
		{
			header('Location: ./error.php?err=Incorrect Activation Token. Send Forgot Pasword Again');
	        exit(1);
		}
	}
	else
	{
		header('Location: ./error.php?err=Connection failed');
	        exit(1);
	}

    if (strlen($password) != 128) 
	{
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Invalid password configuration.</p>';
    }

    // Username validity and password validity have been checked client side.
    // This should should be adequate as nobody gains any advantage from
    // breaking these rules.
    //
    

    if (empty($error_msg)) 
	{
        // Create a random salt
        $random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
        // Create salted password 
        $password = hash('sha512', $password . $random_salt);
		try
		{

			$query1 = "UPDATE members set password = (:pass), salt = (:salt) where email = (:email)";


			$statement1 = $db2->prepare($query1);	
			$params =  array(':pass'=>$password,':salt'=>$random_salt,':email'=>$email);
	 
		    if (! $statement1->execute($params)) 
			{
		        header('Location: ./error.php?err=Reset password failure: UPDATE');
		        exit(1);
		    }
		    else
			{
				header('Location: ./reset_success.php');
				exit(0);
			}
		}
		catch(Exception $e) 
		{
			//echo "<script>alert('dfgdfghdfhfh');</script>";
		    //echo $e->getMessage();
			header('Location: ./error.php?err=Reset password failure: MySQL connection not established');
		}
    }
	else
	{
		echo "<script>alert('error')</script>";
	}
}


?>