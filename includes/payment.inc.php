<?php

include_once '../includes/db_connect.php';
include_once '../includes/psl-config.php';
include_once '../includes/functions.php';

$error_msg = "";

sec_session_start();


if ( isset($_POST['receiver_name'], $_POST['pay_amount'], $_POST['p'], $_POST['balance']) )
{
	//echo "<script>alert('dfdfh');</script>";
	//header("Location: ../error.php?err=hell");
	//exit(0);

	$balance2 = $_POST['balance'];
	$pay_amount2 = $_POST['pay_amount'];
	if (!is_numeric($balance2) || $balance2 < 1 || $balance2 != round($balance2))
	{
		echo "<script>alert('Your Balance is not an integer');</script>";
		exit(0);
	}
	else if (!is_numeric($pay_amount2) || $pay_amount2 < 1 || $pay_amount2 != round($pay_amount2))
	{
		echo "<script>alert('transfer amount is not a non-negative integer');</script>";
		exit(0);
	}
	else
	{
		//echo "<script>alert('an integer');</script>";
	}
    // Sanitize and validate the data passed in
    $receiver_name = filter_input(INPUT_POST, 'receiver_name', FILTER_SANITIZE_STRING);
    $balance = filter_input(INPUT_POST, 'balance', FILTER_SANITIZE_NUMBER_INT);
	$pay_amount = filter_input(INPUT_POST, 'pay_amount', FILTER_SANITIZE_NUMBER_INT);
    
	if ( $pay_amount > $balance )
	{
		header("Location: ../error.php?err=Cannot transfer due to insufficient balance");
        exit(0);
	}
    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
    if (strlen($password) != 128)
    {
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Invalid password configuration.</p>';
    }

    
    

	if ($stmt = $mysqli->prepare("SELECT id, username, password, salt FROM members WHERE username = ? LIMIT 1")) 
	{
        $stmt->bind_param('s', $_SESSION['username']);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();

        // get variables from result.
        $stmt->bind_result($user_id, $sender, $db_password, $salt);
        $stmt->fetch();

        // hash the password with the unique salt.
        $password = hash('sha512', $password . $salt);
        if ($stmt->num_rows == 1) 
		{
            // If the user exists we check if the account is locked
            // from too many login attempts 
            if (checkbrute($user_id, $mysqli) == true) 
			{
                // Account is locked 
                // Send an email to user saying their account is locked 
		//header("Location: ../error.php?Too many login attempts!");
                header("Location: ../error.php?err=Cannot transfer as too many login attempts made");
        		exit(0);
            } 
			else 
			{
                // Check if the password in the database matches 
                // the password the user submitted.
                if ($db_password == $password) 
				{
                    // Password is correct!

                } 
				else 
				{
                    // Password is not correct 
                    // We record this attempt in the database 
                 /*   $now = time();
                    if (!$mysqli->query("INSERT INTO incorrect_attempts(user_id, time) 
                                    VALUES ('$user_id', '$now')")) {
                        header("Location: ../error.php?err=Database error: login_attempts");
                        exit();
                    }
				*/
                    header("Location: ../error.php?err=Incorrect Password");
        			exit(0);
                }
            }
        } 
		else 
		{
            // No user exists. 
            header("Location: ../error.php?err=Sender does not exist!");
        	exit(0);
        }
    } 
	else 
	{
        // Could not create a prepared statement
        header("Location: ../error.php?err=Database error: cannot prepare statement");
        exit(0);
    }


    if (empty($error_msg))
    {

        try
        {
            $db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE .';charset=utf8', USER, PASSWORD);

            $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	    $db2->beginTransaction();



            $query1 = "INSERT INTO transactions (sender, amount, receiver) VALUES (:se, :am, :re)";
			$query2 = "UPDATE members SET balance = balance - (:new_bal) where username = (:sender)";
			$query3 = "UPDATE members SET balance = balance + (:new_bal) where username = (:receiver)";

            $statement1 = $db2->prepare($query1);
			$statement2 = $db2->prepare($query2);
			$statement3 = $db2->prepare($query3);

            $params1 = array(':se'=>$_SESSION['username'],':am'=>$pay_amount,':re'=>$receiver_name);
			$params2 = array(':new_bal'=>$pay_amount,':sender'=>$_SESSION['username']);
			$params3 = array(':new_bal'=>$pay_amount,':receiver'=>$receiver_name);

            if (! $statement1->execute($params1))
            {
                    header('Location: ../error.php?err=Transfer failure:INSERT');
			$db2->rollBack();
                    exit(1);
            }

			if (! $statement2->execute($params2))
            {
                    header('Location: ../error.php?err=Transfer failure:UPDATE');
			$db2->rollBack();
                    exit(1);
            }

			if (! $statement3->execute($params3))
            {
                    header('Location: ../error.php?err=Transfer failure:UPDATE');
			$db2->rollBack();
                    exit(1);
            }
            else
            {
                    header('Location: ../transfer_success.php');
			$db2->commit();
                    exit(0);
            }
		}
		catch( Exception $e)
		{
		        echo $e->getMessage();
			$db2->rollBack();
		        header('Location: ../error.php?err=MySQL Connection failure');
		        exit();
		}
    }
}
