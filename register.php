<?php
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="styles/ssg.css">
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    <meta content="Official website of Slide selector" name="description">
    <title>Slide Selector</title>
    <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
    <link href="styles/ssg.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>
    </head>
    <body>  
    <div id="header">
    <div id="header-content"> &nbsp &nbsp &nbsp Slide Selector </div> </div>
    <div class="clear"></div>
    <div id="menu">
    <div id="menu-content">
        <div id="left-menu">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="register.php">Register</a></li>
            <li><a href="about.html">About Us</a></li>
            <li><a href="contact.html">Contact Us</a></li>    
               
        </div>
    </div>
    </div>  
    <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
 <div class="col-md-offset-1 col-md-5 well" style="margin-top:13%">
        <form class="form-group" method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
            Username: <input type='text'  class="form-control" placeholder="Type your username here" name='username' id='username' /><br>
            Email: <input type="text"  class="form-control" placeholder="Type your email here" name="email" id="email" /><br>
            Password: <input type="password"
                             name="password" 
				 class="form-control" placeholder="Type your password here"
                             id="password"/><br>
            Confirm password: <input type="password" 
                                     name="confirmpwd" 
			 class="form-control" placeholder="Type your password again"
                                     id="confirmpwd" /><br>
            <!--input type="button" 
                   value="Register" 
                   onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd);" /--> 
	    <button type="submit" class="btn btn-primary pull-right" onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd);">Register</button>
        </form>

        <p>Return to the <a href="login_here.php">login page</a>.</p>
	</div>
    <div class="col-md-offset-1 col-md-4 " style="margin-top:13%">    
        <p><font size=6>Hurry up and Register:</font></p>
       
        <ul>
        <font size=5 color="green"><i>
            <li>Usernames have digit & letter.</li>
            <li>Emails must be a valid format.</li>
            <li>Passwords must be:<br>
            At least 6 characters long.<br>
            At least one upper case, lower case and one no.</li>
                </ul>
            </li></i>
            </font>
        </ul>
        </div>

    </body>
</html>
