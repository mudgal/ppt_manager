<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<title>Forbidden 403 error</title>
<style>
<!--
	body {font-family: arial,sans-serif}
	img { border:none; }
//-->
</style>
</head>
<body>
<blockquote>
	<h2>Forbidden 403 : Error</h2>
	<p>Our apologies for the temporary inconvenience. The requested URL was not found on this server.  We suggest you try one of the links below:
	<ul>
		  <li><b>Verify url and typos</b> - The web page you were attempting to view may not exist or may have moved - try <em>checking the web address for typos</em>.<//li>
		  <li><b>E-mail us</b> - If you followed a link from somewhere, please let us know at <a href="mailto:mudgal@iitk.ac.in">mudgal@iitk.ac.in</a>. Tell us where you came from and what you were looking for, and we'll do our best to fix it.</li>
	</ul>
</blockquote>
</body>
</html>
 
