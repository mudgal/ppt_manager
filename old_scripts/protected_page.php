<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Protected Page</title>
        <link rel="stylesheet" href="styles/main.css" />
    </head>
    <body>
        <?php if (login_check($mysqli) == true) : ?>
        <p><font color="red" size=6 style="margin-left:1%"><b>Welcome <?php echo htmlentities($_SESSION['username']); ?>!</b></font></p>
            <p style="margin-left:1%">
                This is your personal protected page.  
            </p>
            
	    <p style="margin-left:1%"> Some links </p>
	    <p style="margin-left:1%">Return to <a href="login_here.php">login page</a></p>
    		<p style="margin-left:1%"><a href="includes/logout.php">Logout</a></p>
		<p style="margin-left:1%">Search required slides <a href="search.php">here</a></p>
	    <p style="margin-left:1%"><a href="file_handler.php">File Handler and Manager</a></p>
	    
	    <div class="col-md-offset-3 col-md-7 well" style="margin-left:10%">
	    <p>My uploaded files</p>
	    <table style="width:130%">
	    <tr>
	    <td>Filename</td>
	    <td>Link</td>
	    <td>UploadTime</td></tr>
	    

	    <?php
	
		define('DB_SERVER', 'localhost');
		define('DB_USERNAME', 'root');
		define('DB_PASSWORD', 'root');
		define('DB_DATABASE', 'secure_login');
		define('DB_TABLE', 'upload_files');
	
		$user_id = htmlentities($_SESSION['user_id']);
		$db2 = new PDO('mysql:host=' .DB_SERVER . ';dbname=' . DB_DATABASE . ';charset=utf8', DB_USERNAME,  DB_PASSWORD);
		$query = "SELECT file, file_link, my_timestamp from " . DB_TABLE . " where owner = (:uid)";
		$parameters2 = array(':uid'=>$user_id);
		$statement1 = $db2->prepare($query);		
		if ( $statement1->execute($parameters2) )
		{
			
			while ($row = $statement1->fetch(PDO::FETCH_ASSOC)) 
			{
				
				echo "<tr><td>".$row['file']."</td>";
				echo "<td><a href='" . $row['file_link'] . "'>" . "Link " . "</a></td>";
				echo "<td>".$row['my_timestamp']."</td></tr>";
				//echo "<br>";
			}
		}
		else
		{
			//echo "print something<br>";
		}

	?>
    </table>
    </div>
    

        <?php else : header( "Location: 403.php" ); ?>
            <p>
                <span class="error">You are not authorized to access this page.</span> Please <a href="login_here.php">login</a>.
            </p>
        <?php endif; ?>






    </body>
</html>
