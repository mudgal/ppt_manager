<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Secure Login: Log In</title>
        <!--link rel="stylesheet" href="styles/main.css" /-->
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script> 
    </head>
    <body background="tag.jpg"> 
    <body>
        <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?> 

        <div class="col-md-offset-3 col-md-6 well" style="margin-top:10%">
            <form class="form-group" action="includes/process_login.php" method="post" name="login_form">           
                Email: <input type="text" name="email" class="form-control" placeholder="Type your email here" /><br/>
                Password: <input type="password"  class="form-control"
                                 name="password" placeholder="Type your password here"
                                 id="password"/><br/>
                <button type="submit" class="btn btn-primary pull-right" onclick="formhash(this.form, this.form.password);">Login</button>
<!--                 <input type="button"
                       value="Login" 
                       onclick="formhash(this.form, this.form.password);" />  -->
            </form>
            <p>If you don't have a login, please <a href="register.php">register</a></p>
            <p>If you are done, please <a href="includes/logout.php">log out</a>.</p>
            <p>You are currently logged <?php echo $logged ?>.</p>

            <p><a href="protected_page.php"> Personal Page </a></p>
        </div>
    </body>
</html>
