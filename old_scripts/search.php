<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
sec_session_start();
if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Search Slides</title>
	</head>
	<body>
	<div class="col-md-offset-3 col-md-5 well" style="margin-top:10%">
            <form class="form-group">
		Enter keywords to search:<br>
		<input type="text" name="search" style="width:50%">
		<button type="submit" class="btn btn-primary pull-right">Search</button>
		</form>
	</div>
	</body>
</html>
