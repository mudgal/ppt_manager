<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<?php
	include_once 'includes/db_connect.php';
	include_once 'includes/functions.php';

	// Include database connection and functions here.  See 3.1. 
	sec_session_start(); 
?>

	<?php if (login_check($mysqli) == true) : ?>
        <p><font color="red" size=6 style="margin-left:1%"><b>Welcome <?php echo htmlentities($_SESSION['username']); ?>!</b></font></p>
            <p style="margin-left:1%">
                This is your file handler page. You can browse through your uploaded files.
            </p>
            
	    <p style="margin-left:1%"> Some links </p>
	    
        <?php else : 
            
	//echo '<p><span class="error">You are not authorized to access this page.</span>';// Please <a href="login_here.php">login</a>.</p>';
	header( "Location: 403.php" );
	exit(0);
	?>
        <?php endif; ?>
<html>
<body>
<div style="margin-left:1%">
<form action="upload_func/file_upload.php" method="post" enctype="multipart/form-data">
<label for="file1">Filename:</label>
<input type="file" class="btn btn-primary" name="upload[]" id="file1" multiple="multiple" style="width:17%"><br>
<input type="submit" class="btn btn-primary" name="submit" value="Submit">
</form>
</div>
<div class="col-md-offset-3 col-md-7 well" style="margin-left:10%">
<p>Here are your files:</p>
<table style="width:130%">
	    <tr>
	    <td>Filename</td>
	    <td>Link</td>
	    <td>UploadTime</td></tr>
	    <?php
	
		define('DB_SERVER', 'localhost');
		define('DB_USERNAME', 'root');
		define('DB_PASSWORD', 'root');
		define('DB_DATABASE', 'secure_login');
		define('DB_TABLE', 'upload_files');
	
		$user_id = htmlentities($_SESSION['user_id']);
		$db2 = new PDO('mysql:host=' .DB_SERVER . ';dbname=' . DB_DATABASE . ';charset=utf8', DB_USERNAME,  
			DB_PASSWORD);
		$query = "SELECT file, file_link, my_timestamp from " . DB_TABLE . " where owner = (:uid)";
		$parameters2 = array(':uid'=>$user_id);
		$statement1 = $db2->prepare($query);		
		if ( $statement1->execute($parameters2) )
		{
			while ($row = $statement1->fetch(PDO::FETCH_ASSOC)) 
			{

				echo "<tr><td>".$row['file']."</td>";
				echo "<td><a href='" . $row['file_link'] . "'>" . "Link " . "</a></td>";
				echo "<td>".$row['my_timestamp']."</td></tr>";
			}
		}
		else
		{}

	?>
</table>
<br><br>
<p>Return to <a href="login_here.php">Login page</a></p>
<p>Go to <a href="protected_page.php">My Own Page</a></p>
<p><a href="includes/logout.php">Logout</a></p>
</div>
</body>
</html> 

