<?php
include_once 'includes/db_connect.php';

error_reporting(0);

$email=$_GET['email'];

$pass_len = mt_rand(10, 30);
$temp_pass = getRandomString($pass_len);
$token_len = mt_rand(60, 100);
$token = getRandomString($token_len);
//$q="insert into tokens (token,email) values ('".$token."','".$email."')";
//mysql_query($q);
function getRandomString($length) 
{
    $validCharacters = "abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNPQRSTUXYVWZ123456789";
    $validCharNumber = strlen($validCharacters);
    $result = "";

    for ($i = 0; $i < $length; $i++) {
        $index = mt_rand(0, $validCharNumber - 1);
        $result .= $validCharacters[$index];
    }
        return $result;
}
function mailresetlink($to,$token,$temp_pass)
{
$subject = "Forgot Password on Doodle (Group 7)";
$uri = 'http://'. $_SERVER['HTTP_HOST'] . '/doodle';
$reset_link = $uri."/reset2.php?token=".$token;
$message = "
<html>
<head>
<title>Forgot Password For Doodle</title>
</head>
<body>
<p>Your Temporary token for activation is : " . $temp_pass . "</p>
<p>Click on the given link to reset your password 
<a href='".$uri."/reset2.php?token=".$token."'>".$reset_link."</a></p>

</body>
</html>
";

/*$message = "Thank you for registering, <br><br>here's a link to download the program:<br>

<a href='http://mywebsite.com/program.progextension'>http://mywebsite.com/program.progextension</a>
<a href='https://localhost/doodle/reset.php'>https://localhost/doodle/reset.php</a><br>
<br><br>Kind Regards,<br>The webmaster";
*/
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
$headers .= 'From: Admin<prg07admin@cs252.cse.iitk.ac.in>' . "\r\n";
$headers .= 'Cc: Admin@example.com' . "\r\n";

$email_valid_status = check_if_valid_email($to);
if ( $email_valid_status == -1 )
{
	header('Location: error.php?err=No such Email id is registered with us');
    exit(1);
}
else if ( $email_valid_status == 0 )
{
	header('Location: error.php?err=Forgot Password Failed Connection error');
    exit(1);
}
else if ( $email_valid_status == 1 )
{
	//header('Location: ../error.php?err=OK');
	//exit(1);
	// email is valid !!
}


$spam_status = check_indirect_spam($to);
if ( $spam_status == -1 )
{
	header('Location: error.php?err=Multiple Forgot Password Requested in short time');
    exit(1);
}
else if ( $spam_status == 0 )
{
	header('Location: error.php?err=Forgot Password Failed Connection error');
    exit(1);
}
else if ( $spam_status == 1 )
{
	//header('Location: ../error.php?err=OK');
	//exit(1);
	// no spamming detected !!
}



$insert_status = insert_token($token, $to, $temp_pass);

if (!$insert_status)
{
	header('Location: error.php?err=Forgot Password Failed');
    exit(1);
}

if(mail($to,$subject,$message,$headers))
//if(1)
{
    //echo $message;
	echo "<br>";
	//echo $uri;
	echo "<br>We have sent the password reset link to your  email id <b>".$to."</b>"; 
	
}
else
{
	print_r(error_get_last());
	echo "Sorry, mail cannot be sent";
}

}

if(isset($_GET['email']))mailresetlink($email,$token,$temp_pass);



function insert_token( $token, $email, $temp_pass )
{
	$db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE .';charset=utf8', USER, PASSWORD);

    $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $query1 = "INSERT INTO reset_attempts (email, token, temp_pass, used_status, time) VALUES (:em, :to, :pa, :tr, :now)";
    $statement1 = $db2->prepare($query1);
	
	$now = time();
	$used_true = 0;
    $params1 = array(':em'=>$email,':to'=>$token,':pa'=>$temp_pass,':tr'=>$used_true,':now'=>$now);
    if (! $statement1->execute($params1))
	{
		return 0;
	}
	return 1;
}

function check_if_valid_email ( $email )
{
	$db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE .';charset=utf8', USER, PASSWORD);

    $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $query1 = "SELECT email FROM members where email = (:em)";
    $statement1 = $db2->prepare($query1);

    $params1 = array(':em'=>$email);
    if (! $statement1->execute($params1))
	{
		return 0;
	}
	else
	{
		if ( $statement1->fetch(PDO::FETCH_NUM) > 0 )
		{
			return 1;
			// email is valid
		}
		else
		{
			return -1;
		}
	}
}

function check_indirect_spam ( $email )
{
	$db2 = new PDO('mysql:host=' .HOST . ';dbname=' . DATABASE .';charset=utf8', USER, PASSWORD);

    $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$now = time();
	$valid_attempts = $now - (60*60);

    $query1 = "SELECT time FROM reset_attempts where email = (:em) and time > (:att)";
    $statement1 = $db2->prepare($query1);

    $params1 = array(':em'=>$email,':att'=>$valid_attempts);
    if (! $statement1->execute($params1))
	{
		return 0;
	}
	else
	{
		$attempts = 0;
		while ($row = $statement1->fetch(PDO::FETCH_ASSOC))
		{ 
			$attempts = $attempts + 1;
			//echo $attempts . "<br>";
		}
		if ( $attempts > 3 )	// more than 2 attempts to reset
		{
			return -1;
			// email is too much used
		}
		else
		{
			return 1;
		}
	}
}

?>
