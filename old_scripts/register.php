<?php
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
        <!--link rel="stylesheet" href="styles/main.css" /-->
    </head>
    <body>
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        <h1>Register with us</h1>
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
        <ul>
            <li>Usernames have only digits, letters and underscores</li>
            <li>Emails must have a valid email format</li>
            <li>Passwords must be at least 6 characters with:
                <ul>
                    <li>At least one upper case (A..Z)</li>
                    <li>At least one lower case (a..z)</li>
                    <li>At least one number (0..9)</li>
                </ul>
            </li>
        </ul>
 <div class="col-md-offset-3 col-md-6 well" style="margin-top:2%">
        <form class="form-group" method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
            Username: <input type='text'  class="form-control" placeholder="Type your username here" name='username' id='username' /><br>
            Email: <input type="text"  class="form-control" placeholder="Type your email here" name="email" id="email" /><br>
            Password: <input type="password"
                             name="password" 
				 class="form-control" placeholder="Type your password here"
                             id="password"/><br>
            Confirm password: <input type="password" 
                                     name="confirmpwd" 
			 class="form-control" placeholder="Type your password again"
                                     id="confirmpwd" /><br>
            <!--input type="button" 
                   value="Register" 
                   onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd);" /--> 
	    <button type="submit" class="btn btn-primary pull-right" onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd);">Register</button>
        </form>

        <p>Return to the <a href="login_here.php">login page</a>.</p>
	</div>
    </body>
</html>
